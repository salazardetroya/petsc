=============================
PETSc |version| Documentation
=============================

.. Important::

  The PETSc docs are currently in transition,
  and different pieces "live" in two different places. In addition to on this page,
  much of the documentation is found `here <https://www.mcs.anl.gov/petsc/documentation/index.html>`__.

.. raw:: html

   <a href="https://my.siam.org/Store/Product/viewproduct/?ProductId=32850137">Ed Bueler, PETSc for Partial Differential Equations: Numerical Solutions in C and Python</a>

   as a <a href="https://play.google.com/store/books/details/Ed_Bueler_PETSc_for_Partial_Differential_Equations?id=tgMHEAAAQBAJ">Google Play E-book</a>

.. toctree::
   :maxdepth: 1

   nutshell
   install/index
   manual/index
   guides/guide_to_examples
   developers/index
   contact/index


.. raw:: html

    <a href="../../../src/binding/petsc4py/docs/usrman/index.html">PETSc4py documentation</a>


.. todolist::
